import dotenv from "dotenv";
dotenv.config();

const config = {
  port: process.env.PORT,
  db: {
    client: "pg",
    connection: process.env.PG_URI,
    migrations: {
      stub: "./src/db/migration.stub",
      directory: "./src/db/migrations",
    },
  },
  security: {
    password: {
      pepper: process.env.SECURITY_PASSWORD_PEPPER,
      keylen: 128,
      iteration: 100000,
      digest: "sha512",
    },
    session: {
      secret: process.env.SECURITY_SESSION_SECRET,
      expiresIn: "2 days",
    },
  },
};
export default config;
