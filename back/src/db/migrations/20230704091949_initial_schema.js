export const up = async (knex) => {
  await knex.schema.createTable("user", function (table) {
    table.increments("id");
    table.text("name", 255).notNullable();
    table.text("email", 255).notNullable().unique();
    table.text("passwordHash").notNullable();
    table.text("passwordSalt").notNullable();
    table.timestamps();
  });

  await knex.schema.createTable("question", function (table) {
    table.increments("id");
    table.text("question_text", 255).notNullable();
    table.timestamps();
  });

  await knex.schema.createTable("answer", function (table) {
    table.increments("id");
    table.text("answer_text", 255).notNullable();
    table.integer("points").unsigned().notNullable().defaultTo(0);
    table.integer("question_id").unsigned().notNullable();
    table
      .foreign("question_id")
      .references("id")
      .inTable("question")
      .onDelete("SET NULL");
    table.timestamps();
  });
  await knex.schema.createTable("user_answer", function (table) {
    table.integer("user_id").unsigned().notNullable();
    table
      .foreign("user_id")
      .references("id")
      .inTable("user")
      .onDelete("SET NULL");
    table.integer("answer_id").unsigned().notNullable();
    table
      .foreign("answer_id")
      .references("id")
      .inTable("answer")
      .onDelete("SET NULL");
    table.integer("qustiion_id").unsigned().notNullable();
    table.timestamps();
  });
  await knex.schema.createTable("score", function (table) {
    table.increments("id");
    table.integer("user_id").unsigned().notNullable();
    table
      .foreign("user_id")
      .references("id")
      .inTable("user")
      .onDelete("SET NULL");
    table.integer("score").unsigned().notNullable();
    table.timestamps();
  });
};

export const down = async (knex) => {
  await knex.schema.dropTable("user_answer");
  await knex.schema.dropTable("answer");
  await knex.schema.dropTable("question");
  await knex.schema.dropTable("score");
  await knex.schema.dropTable("user");
};
