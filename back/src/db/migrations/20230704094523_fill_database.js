export const up = async (knex) => {
  await knex("question").insert([
    { id: 1, question_text: "Défi des déplacements" },
    { id: 2, question_text: "Défi de l'énergie à la maison" },
    { id: 3, question_text: "Défi des voyages aériens" },
    { id: 4, question_text: "Défi de l'alimentation" },
    { id: 5, question_text: "Défi du recyclage" },
    { id: 6, question_text: "Défi de l'eau" },
    { id: 7, question_text: "Défi de la consommation" },
  ]);
  await knex("answer").insert([
    { answer_text: "Je prends le bus", points: 1, question_id: 1 },
    {
      answer_text:
        "Je marche ou je fais du vélo pour la plupart de mes déplacements",
      points: -5,
      question_id: 1,
    },
    {
      answer_text: "Je prends régulièrement les transports en commun",
      points: -3,
      question_id: 1,
    },
    { answer_text: "Je fais du covoiturage", points: -1, question_id: 1 },
    {
      answer_text: "J'utilise une voiture électrique",
      points: 2,
      question_id: 1,
    },
    {
      answer_text:
        "J'utilise une voiture à essence pour la plupart de mes déplacements",
      points: 5,
      question_id: 1,
    },
    {
      answer_text:
        "Mon énergie provient principalement de sources renouvelables",
      points: -5,
      question_id: 2,
    },
    {
      answer_text: "J'utilise principalement de l'électricité",
      points: -1,
      question_id: 2,
    },
    {
      answer_text: "J'utilise principalement du gaz naturel",
      points: 3,
      question_id: 2,
    },
    {
      answer_text: "Mon énergie provient principalement du charbon",
      points: 5,
      question_id: 2,
    },
    { answer_text: "Je ne prends jamais l'avion", points: -5, question_id: 1 },
    {
      answer_text: "Je prends l'avion une fois par an",
      points: -2,
      question_id: 3,
    },
    {
      answer_text: "Je prends l'avion deux fois par an",
      points: 1,
      question_id: 3,
    },
    {
      answer_text: "Je prends l'avion quatre fois par an",
      points: 3,
      question_id: 3,
    },
    {
      answer_text: "Je prends l'avion plus de quatre fois par an",
      points: 5,
      question_id: 3,
    },
    { answer_text: "Je suis végétalien", points: -5, question_id: 4 },
    { answer_text: "Je suis végétarien", points: -3, question_id: 4 },
    {
      answer_text: "Je mange de la viande une fois par semaine",
      points: 1,
      question_id: 4,
    },
    {
      answer_text: "Je mange de la viande tous les jours",
      points: 3,
      question_id: 4,
    },
    {
      answer_text: "Je recycle tous mes déchets recyclables",
      points: -3,
      question_id: 5,
    },
    { answer_text: "Je recycle parfois", points: -1, question_id: 5 },
    { answer_text: "Je ne recycle pas du tout", points: 3, question_id: 5 },
    {
      answer_text:
        "J'utilise des méthodes d'économie d'eau (douche rapide, réutilisation de l'eau, etc)",
      points: -3,
      question_id: 5,
    },
    {
      answer_text: "Je ne prête pas attention à ma consommation d'eau",
      points: 2,
      question_id: 5,
    },
    {
      answer_text:
        "J'achète principalement des produits d'occasion ou durables",
      points: -5,
      question_id: 6,
    },
    {
      answer_text: "J'achète parfois des produits neufs, parfois d'occasion",
      points: 0,
      question_id: 6,
    },
    {
      answer_text: "J'achète principalement des produits neufs",
      points: 4,
      question_id: 6,
    },
  ]);
};

export const down = async (knex) => {
  await knex("answer").del();
  await knex("question").del();
};
